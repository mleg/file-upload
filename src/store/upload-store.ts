import { Module } from 'vuex'
import { api } from '@/fake-api'
import { delay } from '@/common/utils'

export const DOC_TYPES = [
	{ name: 'passport', label: 'Паспорт' },
	{ name: 'drivingLicence', label: 'Водительское удостоверение' },
	{ name: 'taxPayerId', label: 'ИНН' },
]

export const storeOptions: Module<any, any> = {
	namespaced: true,

	state() {
		return {
			docType: '',
			files: [],
			isUploading: false,
			progress: 0,
			validate: false,
		}
	},

	getters: {
		uploadDisabled({ files }) {
			return files.length === 0
		},

		fileList({ files }) {
			return files.map(({ name, uid }) => ({ name, uid }))
		},

		progressStatus({ progress, isUploading }) {
			return progress === 100 && isUploading ? 'success' : ''
		},

		isFileListEmpty({ files }) {
			return files.length === 0
		},

		inputErrors({ docType, validate }) {
			const errors = {}
			if (validate && !docType) {
				errors['docType'] = 'Необходимо указать тип документа'
			}
			return errors
		},

		isError(state, getters) {
			return Object.keys(getters.inputErrors).length > 0
		},
	},

	mutations: {
		setDocType(state, docType) {
			state.docType = docType
		},

		addFile(state, file) {
			state.files.push(file)
		},

		clearFiles(state) {
			state.files = []
		},

		start(state) {
			state.isUploading = true
		},

		finish(state) {
			state.isUploading = false
		},

		setProgress(state, progress) {
			state.progress = progress
		},

		setValidate(state, validate) {
			state.validate = validate
		},
	},

	actions: {
		async uploadFiles({ state, commit, getters }) {
			commit('setProgress', 0)
			commit('setValidate', true)
			if (getters.isError) {
				return ''
			}
			const onProgress = progress => {
				commit('setProgress', progress)
			}
			commit('start')
			const success = await api.uploadFiles(
				state.files,
				onProgress,
			)
			await delay(700) // To show progress in success status
			commit('finish')
			if (success) {
				commit('files/storeFiles', state, { root: true })
				commit('setValidate', false)
				commit('clearFiles')
			}
			return success ? 'success' : 'error'
		},
	},
}

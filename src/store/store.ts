import Vue from 'vue'
import Vuex from 'vuex'
import { storeOptions as upload } from './upload-store'
import { storeOptions as files } from './files-store'

Vue.use(Vuex)

export const store = new Vuex.Store({
	modules: {
		upload,
		files,
	},
})

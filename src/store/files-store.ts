import { Module } from 'vuex'
import Vue from 'vue'

export const storeOptions: Module<any, any> = {
	namespaced: true,

	state() {
		return {
			storedFiles: [],
			filesById: {},
		}
	},

	mutations: {
		storeFiles(state, { files, docType }) {
			const start = state.storedFiles.length
			files.forEach((file, i) => {
				Vue.set(state.filesById, String(file.uid), start + i)
			})

			const toItem = file => ({
				file,
				docType,
			})
			state.storedFiles.push(...files.map(toItem))
		},

		changeDocType(state, payload: { uid: string; docType: string }) {
			const item = state.storedFiles[state.filesById[payload.uid]]
			Vue.set(item, 'docType', payload.docType)
		},

		removeFile(state, uid: string) {
			const index = state.filesById[uid]
			state.storedFiles.splice(index, 1)
			delete state.filesById[uid]
			for (let i = index; i < state.storedFiles.length; i += 1) {
				const currentUid = String(state.storedFiles[i].file.uid)
				state.filesById[currentUid] = i
			}
		},
	},
}

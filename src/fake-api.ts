import { delay } from '@/common/utils'

export const api = {
	async uploadFiles(files, onProgress) {
		for (let progress = 0; progress <= 100; progress += 5) {
			await delay(40)
			onProgress(progress)
		}
		return true
	},
}

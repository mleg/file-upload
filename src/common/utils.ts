export function delay(ms, optValue?) {
	return new Promise(resolve => {
		setTimeout(() => resolve(optValue), ms)
	})
}

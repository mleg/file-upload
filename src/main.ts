import 'normalize.css/normalize.css'
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/global.css'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store/store'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'

Vue.config.productionTip = false

Vue.use(ElementUI, { locale })

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
